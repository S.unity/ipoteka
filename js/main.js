const sendMessHead = document.getElementById('sendMessHead')
const sendMessFoot = document.getElementById('sendMessFoot')

async function send (nameId, emailId, phoneId, siteName, btn, info = undefined) {
  // отправка на срм
  const name = document.getElementById(nameId).value
  let email
  emailId ? email = document.getElementById(emailId).value : email = ''
  const phone = document.getElementById(phoneId).value
  let infoForm = null
  info ? infoForm = document.getElementById(info).value : null
  const sendMessBtn = document.getElementById(btn)
  if (
    name.length &&
    phone.length
  ) {
    sendMessBtn.innerText = 'Подождите...'
    const formData = new FormData()
    formData.append('crm', '16')
    formData.append('pipe', '34')
    formData.append('contact[name]', name)
    formData.append('contact[466]', phone)
    formData.append('contact[467]', email)
    formData.append('lead[541]', siteName)
    formData.append('note', 'Заявка с ' + siteName + '<br>' + 'Дополнительно: ' + infoForm)
    await axios.post('https://bez.v-avtoservice.com/api/import-lead',
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    ).then(res => {
      if (res.data.text !== 'Все данные успешно импортированы') {
        console.log(res.data.link.result.error_message)
        alert('ошибка, попробуйте позже')
      } else {
        Swal.fire({
          title: '<strong>Спасибо!</strong>',
          icon: 'success',
          html:
              'Мы свяжемся с Вами и ответим на все Ваши вопросы.<br><br>Выберите удобный способ для связи:<div style="margin-top: 30px;"><a href="https://wa.me/79250194968" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="https://avtourist.com/img/soc/wa.jpg" alt="WhatsApp" style="width: 120px;"></a><a href="tg://resolve?domain=Avtourist_bot" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="https://avtourist.com/img/soc/tg.jpg" alt="Telegram" style="width: 120px;"></a><a href="viber://pa?chatURI=avtourist-bot&context=welcome" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="https://avtourist.com/img/soc/vb.jpg" alt="Viber" style="width: 120px;"></a><a href="https://vk.com/write-7375007" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="https://avtourist.com/img/soc/vk.jpg" alt="VK" style="width: 120px;"></a></div></div>',
          showCloseButton: true,
          focusConfirm: false,
          showConfirmButton: false
        })
        sendMessBtn.innerText = 'Заявка отправлена!'
        sendMessBtn.style.cssText = 'pointer-events: none'
      }
    })
  } else {
    sendMessBtn.innerText = 'Заполните все поля!'
    setTimeout(() => {
      sendMessBtn.innerText = 'Отправить'
    }, 2500)
  }
}

// в зависимости от того, на какую кнопку нажали, применяются разные аргументы для функции sendMess()
sendMessHead.addEventListener('click', (e) => {
  e.preventDefault()
  if (sendMessHead === e.target) {
    send('head-name', 'head-email', 'head-phone', 'ипотека.мфюц.рф', 'sendMessHead', 'head-info')
  }
})
sendMessFoot.addEventListener('click', (e) => {
  e.preventDefault()
  if (sendMessFoot === e.target) {
    send('foot-name', 'foot-email', 'foot-phone', 'ипотека.мфюц.рф', 'sendMessFoot', 'foot-info')
  }
})

//Анимация плавного появления блоков
const animItems = document.querySelectorAll('.anim-items');
if (animItems.length > 0) {
  window.addEventListener('scroll', animOnScroll);

  function animOnScroll(){
    for (let i = 0; i < animItems.length; i++) {
      const animItem = animItems[i];
      const animItemHeight = animItem.offsetHeight;
      const animItemOffset = offset(animItem).top;
      const animStart = 10;
      //moment nachala animation
      let animItemPoint = window.innerHeight - animItemHeight / animStart; //moment nachala animation
      if (animItemHeight > window.innerHeight) {
        animItemPoint = window.innerHeight - animItemHeight / animStart;
      };
      //moment polucheniya class
      if ((pageYOffset > animItemOffset - animItemPoint) && pageYOffset < (animItemOffset + animItemHeight)) {
        animItem.classList.add('anim');
      } else {
        if (!animItem.classList.contains('anim-no-hide')) {
          animItem.classList.remove('anim');
        };
      };
    };
  }; // Анимация плавного появления блоков


  function offset(el){
    const rect = el.getBoundingClientRect(),
      scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
  };
  setTimeout( () => {
    animOnScroll();
  }, 300);
};

// закрытие попап "месенджеры"
// function closedPopUp () {
//   document.getElementById('modalSubscribe').style.display = 'none'
// }
